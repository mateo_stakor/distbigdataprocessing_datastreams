package rovkp4dz;

public class SensorscopeReading {
    public int stationId;
    public int year;
    public int month;
    public int day;
    public int hour;
    public int minute;
    public int second;
    public int timeSinceEpoch;
    public int sequenceNumber;
    public double configSamplingTime;
    public double dataSamplingTime;
    public double radioDutyCycle;
    public double radioTransmissionPower;
    public double radioTransmissionFreq;
    public double primaryBufferVoltage;
    public double secondaryBufferVoltage;
    public double solarPanelCurrent;
    public double globalCurrent;
    public double energySource;
}
