package rovkp4dz;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App
{
    public static void main( String[] args ) throws IOException {
        if (args.length != 1) {
            System.err.println(String.format("usage: <input_folder_path>"));
            System.exit(1);
        }

        try(FileWriter writer = new FileWriter("C:\\Users\\mateo\\Desktop\\sensorscope-monitor-all.csv");
            BufferedWriter bw = new BufferedWriter(writer)) {

            try {
                Files.list(Paths.get(args[0]))
                        .flatMap(path -> {
                            try {
                                return Files.lines(path);
                            } catch (IOException e) {
                                System.err.println(String.format("Could not read file content from: %s", path.toString()));
                                return null;
                            }
                        })
                        .map(App::parse)
                        .filter(Objects::nonNull)
                        .sorted(Comparator.comparing(s -> s.timeSinceEpoch))
                        .map(App::toCSVRow)
                        .filter(s -> !s.isEmpty())
                        .forEach(l -> {
                            try {
                                bw.write(l);
                                bw.newLine();
                            } catch (IOException ignored) {}
                        });
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                bw.close();
            }
        }
    }

    private static SensorscopeReading parse(String line) {
        String[] splitted = line.split(" ");
        if (splitted.length != 19) return null;

        try {
            SensorscopeReading s = new SensorscopeReading();
            s.stationId = Integer.parseInt(splitted[0]);
            s.year = Integer.parseInt(splitted[1]);
            s.month = Integer.parseInt(splitted[2]);
            s.day = Integer.parseInt(splitted[3]);
            s.hour = Integer.parseInt(splitted[4]);
            s.minute = Integer.parseInt(splitted[5]);
            s.second = Integer.parseInt(splitted[6]);
            s.timeSinceEpoch = Integer.parseInt(splitted[7]);
            s.sequenceNumber = Integer.parseInt(splitted[8]);
            s.configSamplingTime = Double.parseDouble(splitted[9]);
            s.dataSamplingTime = Double.parseDouble(splitted[10]);
            s.radioDutyCycle = Double.parseDouble(splitted[11]);
            s.radioTransmissionPower = Double.parseDouble(splitted[12]);
            s.radioTransmissionFreq = Double.parseDouble(splitted[13]);
            s.primaryBufferVoltage = Double.parseDouble(splitted[14]);
            s.secondaryBufferVoltage = Double.parseDouble(splitted[15]);
            s.solarPanelCurrent = Double.parseDouble(splitted[16]);
            s.globalCurrent = Double.parseDouble(splitted[17]);
            s.energySource = Double.parseDouble(splitted[18]);

            return s;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static String toCSVRow(SensorscopeReading s) {
        if (s != null) {
            return String.format("%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f",
                    s.stationId, s.year, s.month, s.day, s.hour, s.minute, s.second, s.timeSinceEpoch, s.sequenceNumber,
                    s.configSamplingTime, s.dataSamplingTime, s.radioDutyCycle, s.radioTransmissionPower,
                    s.radioTransmissionFreq, s.primaryBufferVoltage, s.secondaryBufferVoltage, s.solarPanelCurrent,
                    s.globalCurrent, s.energySource);
        }

        return "";
    }
}
