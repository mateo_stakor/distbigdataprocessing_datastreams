package rovkp4dz;

import java.util.NoSuchElementException;
import java.util.Objects;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;

public class App2
{
    public static void main( String[] args )
    {
        if (args.length != 1) {
            System.out.println("Usage: App2 <input file>");
            System.exit(-1);
        }

        SparkConf conf = new SparkConf().setAppName("Rovkp4DZ-zad2");

        //set the master if not already set through the command line
        try {
            conf.get("spark.master");
        } catch (NoSuchElementException ex) {
            //spark streaming application requires at least 2 threads
            conf.setMaster("local");
        }

        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> lines = sc.textFile(args[0]);
        JavaRDD<USBabyNameRecord> result = lines
                .map(App2::parse)
                .filter(Objects::nonNull)
                .persist(StorageLevel.MEMORY_ONLY());

        USBabyNameRecord first = result.first();
        System.out.println("First is: " + first.id);
    }

    public static USBabyNameRecord parse(String line) {
        if (line.split(",").length != 6) return null;
        else {
            try {
                USBabyNameRecord b = new USBabyNameRecord();
                String[] splitted = line.split(",");
                b.id = Integer.parseInt(splitted[0]);
                b.name = splitted[1];
                b.year = Integer.parseInt(splitted[2]);
                b.gender = splitted[3].charAt(0);
                b.state = splitted[4];
                b.count = Integer.parseInt(splitted[5]);
                return b;
            } catch (Exception e) {
                return null;
            }
        }
    }
}
